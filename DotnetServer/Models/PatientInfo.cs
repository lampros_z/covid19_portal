using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DotnetServer.Models
{
    public class PatientInfo
    {
        public int Id {get;set;}
        
        [Required]
        [RegularExpression(@"^\d{11}$", ErrorMessage = "Invalid Social Security Number")]
        public string SSN {get;set;}

        [Required]
        [RegularExpression(@"^[a-zA-Z]{4,}(?: [a-zA-Z]+){0,2}$", ErrorMessage = "Invalid full name")]
        public string Fullname {get;set;}

        [Required]
        [RegularExpression(@"^(\+30)?\d{10}$", ErrorMessage = "Invalid phone number")]
        public string Phone {get;set;}

        [Required]
        public string Mail {get;set;}

        [Required]
        public string BirthDate {get;set;}

        [Required]
        [MaxLength(100)]
        [RegularExpression(@"^[A-Za-z0-9]+(?:\s[A-Za-z0-9'_-]+)+$", ErrorMessage = "Invalid address.")]
        public string Address {get;set;}

        public ICollection<Members> Members {get;set;}
    }
}