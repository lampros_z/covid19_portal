using System.ComponentModel.DataAnnotations;

namespace DotnetServer.Models
{
    public class Members
    {
        public int Id {get;set;}
        
        [Required]
        [RegularExpression(@"^\d{11}$", ErrorMessage = "Invalid Social Security Number")]
        public string SSN {get;set;}

        public int PatientInfoId { get; set; }
        public PatientInfo patientInfo { get; set; }
    }
}