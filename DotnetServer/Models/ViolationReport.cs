using System.ComponentModel.DataAnnotations;

namespace DotnetServer.Models
{
    public class ViolationReport
    {
        public int Id {get;set;}
        
        [Required]
        [MaxLength(100)]
        [RegularExpression(@"^[A-Za-z0-9]+(?:\s[A-Za-z0-9'_-]+)+$", ErrorMessage = "Invalid address.")]
        public string Address {get;set;}
        
        [Required]
        [MaxLength(500)]
        public string Details {get;set;}
    }
}