﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DotnetServer.Migrations
{
    public partial class secondMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PhoneNumber",
                table: "patientInfos");

            migrationBuilder.AddColumn<string>(
                name: "Mail",
                table: "patientInfos",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Phone",
                table: "patientInfos",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Mail",
                table: "patientInfos");

            migrationBuilder.DropColumn(
                name: "Phone",
                table: "patientInfos");

            migrationBuilder.AddColumn<string>(
                name: "PhoneNumber",
                table: "patientInfos",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }
    }
}
