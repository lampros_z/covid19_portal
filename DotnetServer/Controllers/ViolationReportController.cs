using DotnetServer.Data;
using DotnetServer.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace DotnetServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ViolationReportController:ControllerBase
    {
        
        private readonly IDotNetServerRepository _repository;

        /*Constructor based dependency injection*/
        public ViolationReportController(IDotNetServerRepository repository){
            _repository = repository;  
        }

        [HttpPost]
        public ActionResult<ViolationReport> createViolationReport([FromBody]ViolationReport violationReport){
            //validation here
            _repository.createViolationReport(violationReport);
            _repository.SaveChanges();
            //Should return created at
            return Ok();
        }

    }
}