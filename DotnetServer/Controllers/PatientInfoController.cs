using DotnetServer.Data;
using DotnetServer.Models;
using Microsoft.AspNetCore.Mvc;

namespace DotnetServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PatientInfoController:ControllerBase
    {
        
        private readonly IDotNetServerRepository _repository;

        /*Constructor based dependency injection*/
        public PatientInfoController(IDotNetServerRepository repository){
            _repository = repository;  
        }

        [HttpPost]
        public ActionResult<PatientInfo> createViolationReport([FromBody]PatientInfo patientInfo){
            //validation here
            _repository.createPatientInfo(patientInfo);
            _repository.SaveChanges();
            //Should return created at
            return Ok();
        }

    }
}