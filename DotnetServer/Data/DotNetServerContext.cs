using DotnetServer.Models;
using Microsoft.EntityFrameworkCore;

namespace DotnetServer.Data
{
    public class DotNetServerContext:DbContext
    {
        
        public DotNetServerContext(DbContextOptions<DotNetServerContext> options) : base(options)
        {

        }

        public DbSet<ViolationReport> violationReports {get;set;}
        public DbSet<PatientInfo> patientInfos {get;set;}
    }
}