using System;
using System.Collections.Generic;
using System.Linq;
using DotnetServer.Models;

namespace DotnetServer.Data
{
    public class SqlDotNetServerRepo : IDotNetServerRepository
    {
        private readonly DotNetServerContext _context;
        /*Constructor based dependency injection*/
        public SqlDotNetServerRepo(DotNetServerContext context){
            _context = context;
        }

        public void createPatientInfo(PatientInfo patientInfo)
        {
            if(patientInfo == null){
               throw new ArgumentNullException(nameof(patientInfo));
           }
           _context.patientInfos.Add(patientInfo);
        }

        public void createViolationReport(ViolationReport violationReport)
        {
            if(violationReport == null){
               throw new ArgumentNullException(nameof(violationReport));
           }
           _context.violationReports.Add(violationReport);
        }

        public IEnumerable<ViolationReport> GetAllViolationReports()
        {
            return _context.violationReports.ToList();
        }

        public IEnumerable<PatientInfo> GetAllVPatientsInfo()
        {
            return _context.patientInfos.ToList();
        }

        public PatientInfo GetPatientInfoById(int id)
        {
            return _context.patientInfos.FirstOrDefault(p => p.Id == id);
        }

        public ViolationReport GetViolationReportById(int id)
        {
            return _context.violationReports.FirstOrDefault(p => p.Id == id);
        }

        public bool SaveChanges()
        {
            return (_context.SaveChanges() >= 0);
        }
    }
}