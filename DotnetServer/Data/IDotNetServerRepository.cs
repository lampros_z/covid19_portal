using System.Collections.Generic;
using DotnetServer.Models;

namespace DotnetServer.Data
{
    public interface IDotNetServerRepository
    {
        bool SaveChanges();
        IEnumerable<PatientInfo> GetAllVPatientsInfo();
        PatientInfo GetPatientInfoById(int id);
        void createPatientInfo(PatientInfo patientInfo);
        IEnumerable<ViolationReport> GetAllViolationReports();
        ViolationReport GetViolationReportById(int id);
        void createViolationReport(ViolationReport violationReport);
    }
}