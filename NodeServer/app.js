// index.js

/**
 * Required External Modules
 */

const express = require("express");
const path = require("path");
const ejs = require('ejs');
const bodyParser = require('body-parser');
const request = require('request')

/**
 * App Variables
 */

const app = express();
const port = process.env.PORT || "8000";
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0;

/**
 *  App Configuration
 */
app.set('view engine', 'ejs');
app.use(express.static('static'));
app.use(bodyParser.json());

/**
 * Routes Definitions
 */
app.get("/", (req, res) => {
  res.status(200).render("index.ejs");
});

app.get("/reportViolation", (req, res) => {
  res.status(200).render("report_form.ejs");
});

app.get("/registerCase", (req, res) => {
  res.status(200).render("register_form.ejs");
});

app.post("/reportViolation", (req, res) => {
  request.post('http://localhost:5000/api/violationReport', {
    json: req.body
  }, (error, response, body) => {
    if (error) {
      console.error(error)
      return
    }
    
    console.log(`statusCode: ${response.statusCode}`)
    res.status(response.statusCode).send(JSON.stringify({
      "result": code
    }));
  })

});

app.post("/registerCase", (req, res) => {
  console.log(req.body);
  request.post('http://localhost:5000/api/patientInfo', {
    json: req.body
  }, (error, response, body) => {
    if (error) {
      console.error(error)
      return
    }
    
    console.log(`statusCode: ${response.statusCode}`)
    res.status(response.statusCode).send(JSON.stringify({
      "result": response.statusCode
    }));
  })


});

/**
 * Server Activation
 */
app.listen(port, () => {
  console.log(`Listening to requests on http://localhost:${port}`);
});
