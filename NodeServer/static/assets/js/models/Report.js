class Report{
    constructor(address, details){
        this.address = address;
        this.details = details;
    }

    getAddress = ()  => {
        return this.address;
    }

    getDetails = () => {
        return this.details;
    }

    setAddress = (address) => {
        this.address = address;
    }

    setDetails = (details) => {
        this.details = details;
    }
}

document.getElementById('submit-btn-1').addEventListener('click', function () {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', url, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    let address = document.getElementById('address_input').value;
    let details = document.getElementById('details_text').value;
    let report = new Report(address, details);
    xhr.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            console.log(JSON.parse(xhr.responseText));    
        }
    }
    xhr.send(JSON.stringify(report));
});