class Patient{
    constructor(ssn, fullname, phone, mail, birthdate, address, members){
        this.ssn = ssn;
        this.fullname = fullname;
        this.phone = phone;
        this.mail = mail;
        this.birthdate = birthdate;
        this.address = address;
        this.members = members;
    }

    getSSN = () => {
        return this.ssn;
    }

    getFullName = () => {
        return this.fullname;
    }

    getPhone = () => {
        return this.phone;
    }

    getMail = () => {
        return this.mail;
    }

    getBirthdate = () => {
        return this.birthdate;
    }

    getAddress = () => {
        return this.address;
    }

    getNoOfPeople = () => {
        return this.members;
    }

    setSSN = (ssn) => {
        this.ssn = ssn;
    }

    setFullName = (fullname) => {
        this.fullname = fullname;
    }

    setPhone = (phone) => {
        this.phone = phone;
    }

    setMail = (mail) => {
        this.mail = mail;
    }

    setBirthDate = (birthdate) => {
        this.birthdate = birthdate;
    }

    setAddress = (address) => {
        this.address = address;
    }

    setNoOfPeople = (members) => {
        this.members = members;
    }
}

document.getElementById('submit-btn-2').addEventListener('click', function () {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', url, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    let ssn = document.getElementById('ssnInput').value;
    let fullname = document.getElementById('fullNameInput').value;
    let phone = document.getElementById('phoneInput').value;
    let mail = document.getElementById('mailInput').value;
    let birthdate = document.getElementById('dateInput').value;
    let address = document.getElementById('addressInput').value;
    let members = [];
    
    let children = document.getElementById('container').childElementCount;
    if( children > 0 ){
        for(let i = 0; i < children; i++){
            let obj = new Object();
            obj["ssn"] = document.getElementById('container').childNodes[i].childNodes[0].childNodes[1].value;
            members.push(obj);
        }
    }

    let patient = new Patient(ssn, fullname, phone, mail, birthdate, address, members);
    xhr.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            console.log(JSON.parse(xhr.responseText));    
        }
    }
    xhr.send(JSON.stringify(patient));
});