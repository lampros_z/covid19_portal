const url = window.location.href; 



function setUrls(){
    document.getElementById('card1anchor').setAttribute('href', url+'registerCase');
    document.getElementById('card2anchor').setAttribute('href', url+'');
    document.getElementById('card3anchor').setAttribute('href', url+'reportViolation');
}



function addFields(){
    let number = document.getElementById("people_socialised_with").value;
    let container = document.getElementById("container");
    while (container.hasChildNodes()) {
        container.removeChild(container.lastChild);
    }
    for (let i=0;i<number;i++){

        let span = document.createElement("span");
        span.setAttribute('class', 'uk-form-icon');
        span.setAttribute('uk-icon', 'icon: tag');

        let input = document.createElement("input");
        input.setAttribute('type', 'text');
        input.setAttribute('class', 'uk-input');
        input.setAttribute('id', 'member'+(i+1));
        input.setAttribute('placeholder', 'SSN ' + (i+1));
        
        let inlineDiv = document.createElement("div");
        inlineDiv.setAttribute('class', 'uk-inline');
        inlineDiv.appendChild(span);
        inlineDiv.appendChild(input);

        let marginDiv = document.createElement("div");
        marginDiv.setAttribute('class', 'uk-margin');
        marginDiv.appendChild(inlineDiv);
        container.appendChild(marginDiv);
    }
}